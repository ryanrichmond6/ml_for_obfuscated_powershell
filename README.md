# ML_for_Obfuscated_PowerShell

This repository is a posting from the CYBV 474: Advanced Analytics for Security Professionals course offered through Universtiy of Arizona. 
Instructor: Chet Hosmer

The final was an open ended project prompted with the instruction to create a security related machine learning model for use by security professionals. 

## Design

The first result utilized a Naive-Bayes model and language parsing to detect obfuscated PowerShell. The second model utilized sci-kit learn models in an attempt to match the accuracy of a NB supervised model. 

## Use Case

The idea behind the project originated from working in Incident Response and attempting to triage Windows systems from the Emotet campaigns. The Emotet malware utilized a framework of tools that often leveraged PowerShell and Batch obfuscation for deployment and persistence. 
The obfuscated PowerShell would step past detection engines of various EDR solutions but it was plain to see in the PowerShell-Operational, and Securty logs that there was malicious activity present. 

### Future Uses

Hopefully the community can continue developing simple detection ML solutions that can be deployed alongside EDR models to examine EVTX file for malicious activity and assist in detecting malware, lateral movement or persistence mechanisms through obfuscation detection. 

