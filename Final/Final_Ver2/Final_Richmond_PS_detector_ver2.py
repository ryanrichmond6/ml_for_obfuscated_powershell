'''

Script Version: 1.2 May 7 2021
Script Author:  Ryan Richmond

'''

# Script Module Importing

# Python Standard Library Modules
import os           # Operating/Filesystem Module
import time         # Basic Time Module
import random

# Import 3rd Party Modules
''' importing of any required 3rd party libraries '''
import nltk
#from nltk.corpus import names
from nltk import NaiveBayesClassifier,classify, accuracy
from collections import Counter

import numpy as np                  # pip install numpy
import matplotlib.pyplot as plt     # pip install matplotlib

from sklearn import datasets        # pip install scikit-learn
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import KNeighborsRegressor

from sklearn import preprocessing
# End of Script Module Importing

# Script Constants
# Psuedo Constants
SCRIPT_NAME    = "Script: Obfuscated PowerShell Detector"
SCRIPT_VERSION = "Version 1.2"
SCRIPT_AUTHOR  = "Author: Ryan Richmond"
# Powershell Files 
OBFUSC_PS = "Obfusc_PS.txt"
PLAINTEXT_PS = "PlainText_PS.txt"
TEST_PS = "Test_PS.txt"
# End of Script Constants

#PSUEDO GLOBALS CONSTANTS 
features = []
scriptType = []
# Script Functions
def GetTime(timeStyle = "UTC"):
    '''
    Function: GetTime()
    Input timeStyle either UTC or LOCAL
          UTC is the default if not argument is provided
    Returns a string containing the current time
    
    Description:
    Script will use the local system clock, time, date and timezone
    to calcuate the current time.  Thus you should sync your system
    clock before using this script

    '''
    epochValue = time.time()
    
    if timeStyle == 'UTC':
        utcTime = time.gmtime(epochValue)
        timeString = time.asctime(utcTime)
        return 'UTC Time: '+ timeString
    elif timeStyle == 'LOCAL':
        localTime = time.localtime(epochValue)
        timeString = time.asctime(localTime)  
        return 'Local Time: '+ timeString
    else:
        return "Invalid TimeStyle Specified"
    
# End GetTime Function       

# Gender Features Function 
def powershellFeatures(theScript): 
    # Feature Sets
    upperChar = 0
    lowerChar = 0
    alphaChar = 0
    digitChar = 0
    symbolChar = 0
    otherChar = 0
    
    last10Letters    = theScript[-10:]
    
    symbolsList = '!@#$%^&*()_-+=,./<>;[]{}|'
    
    frequencyPerChar = Counter(theScript)
    for key,value in frequencyPerChar.items():
        if key.isupper():
            upperChar +=1
        elif key.islower():
            lowerChar +=1
        elif key.isalpha():
            alphaChar +=1
        elif key.isdigit():
            digitChar +=1
        if not key.isalnum():
            #emphasize symbol
            symbolChar +=2
        else:
            #emphasize other
            otherChar+=3
    
    theScriptLength  = len(theScript)
    #emphasize the length of script for weight 
    theScriptLength = (theScriptLength)*2
    
    digitChar10 = 0
    alphaChar10 = 0
    otherChar10 = 0
    frequencyPerCharLast10 = Counter(last10Letters)
    for key,value in frequencyPerCharLast10.items():  
        if key.isdigit():
            digitChar10 +=1
        if key.isalpha():
            alphaChar10 +=1
        else:
            otherChar10 +=1
    lastRatio = ((otherChar10) / (digitChar10 + alphaChar10)) * 100
    lastRatio = lastRatio*(1.25)
    # get percentage of symbols versus letters? 
    #symbolChar = ( (digitChar+symbolChar) // (upperChar+lowerChar+alphaChar) ) * 100
    # from NLTK Ch 6: 
    '''
    >>> def gender_features(word):
    ...     return {'suffix1': word[-1:],
    ...             'suffix2': word[-2:]}
    '''
    #last5Letters     = theScript[-5:]
    
    #first5Letters    = theScript[0:5]
    return upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio

# End of Script Functions

# Script Classes
# End of Script Classes

# Main Script Starts Here

def main():
    
    # Print Basic Script Information
    print()
    print(SCRIPT_NAME)
    print(SCRIPT_VERSION)
    print(SCRIPT_AUTHOR)
    print()
    
    localTime = GetTime('LOCAL')
    print("Local Time:   ", localTime)

    utcTime = GetTime('UTC')
    print("UTC Time:     ", utcTime)  
    
    print("This python script ingests two files. One file contains one-liner useful PowerShell commands which a system administrator would run on their system."
          "The second file contains the same one-liners but obfuscated utilizing the Invoke-Obfuscation public resource."
          "The obfuscated PowerShell commands are obfuscated utilizing different methods, such as string obfuscation, function obfuscation, data encoding, and reversing"
          "Then, this python script will extract features from the two sets of PowerShell commands and perform a Naive Bayes classification to attempt to identify if the PowerShell was obfuscated or not.")
    
    
    # read the powershell files into memory
    with open(OBFUSC_PS, "r") as obfuscFile:
        obfuscPS = obfuscFile.read().splitlines()
    
    with open(PLAINTEXT_PS, "r") as plaintxtFile:
        plainTxtPS = plaintxtFile.read().splitlines()
        
    with open(TEST_PS, "r") as testFile:
        testFilePS = testFile.read().splitlines()
        
    print("\nProcessing Obfuscated Samples ... \n")
    for entry in obfuscPS:
        upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio = powershellFeatures(entry)
        features.append([upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio])
        scriptType.append(0) # append 0 for obfuscated 
        
    print("\nProcessing Plain Text Samples ... \n")
    for entry in plainTxtPS:
        upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio = powershellFeatures(entry)
        features.append([upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio])    
        scriptType.append(1) # append 1 for plain Text
    
    '''
    # Collect Name Label 
    powerShellLabels = ([(script, 'obfusc') for script in obfuscPS] + [(script, 'plain') for script in plainTxtPS])
    #shuffle the order so that we can take a sample with array index
    random.shuffle(powerShellLabels)    
    
    # total size of nameLabels
    sampleSize = len(powerShellLabels)
    # trainingSize is 75% of sampleSize
    trainingSize = int(sampleSize * .75)
    # testSize is the remainder (25%)
    testSize = sampleSize - trainingSize
    
    # Create a  training Feature set
    trainingFeatureSet = [(powershellFeatures(n), value) for (n, value) in powerShellLabels[0:trainingSize]]
    # Create a testing Feature Set 
    testingFeatureSet  = [(powershellFeatures(n), value) for (n, value) in powerShellLabels[trainingSize:]]  
    
    # Create a NaiveBayes Gender Classifer from the Training Set
    obfuscatorClassifer = NaiveBayesClassifier.train(trainingFeatureSet) 
    
    
    # Print Accuracy Results 
    print("="*80)
    print()
    print('TrainSet Accuracy: ',classify.accuracy(obfuscatorClassifer, trainingFeatureSet)) 
    print('TestSet  Accuracy: ',classify.accuracy(obfuscatorClassifer, testingFeatureSet),"\n")
    
    # print most statistically sig features 
    print("="*80)
    print()
    obfuscatorClassifer.show_most_informative_features(10)
    '''
    
    print("\nCreating Nearest Neighbor Model\n")
    
    # Create the Model 
    model = KNeighborsRegressor(n_neighbors=1)
    model.fit(features, scriptType)
    
    # testing features for the model 
    tstFeatures = []
    
    print("\nTesting the Model\n")
    for testScript in testFilePS:
        upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio = powershellFeatures(testScript)
        tstFeatures.append([upperChar, lowerChar, alphaChar, digitChar, symbolChar, otherChar, theScriptLength, lastRatio])
        
    print("="*80)
    print("\nPrediction", "\t", "Script \n")
    cnt = 0
    for elem in tstFeatures:
        scriptNum = testFilePS[cnt]
        cnt+=1
        p = model.predict([elem])
        if p == 0:
            print("Obfuscated: ", "\t", scriptNum)
        elif p == 1:
            print("PlainText: ", "\t", scriptNum)
        else:
            print("None: ", "\t", scriptNum)
    
    
    
    print("\nScript End")   
# Main Program Starts Here
#===================================

if __name__ == '__main__':
    main()
    
# End of Script Main
